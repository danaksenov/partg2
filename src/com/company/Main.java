package com.company;
import java.util.Scanner;
import com.company.model.Book;
import com.company.model.Magazine;
import com.company.model.Yearbook;

public class Main {

    public static void main(String[] args) {

        Magazine magazine = new Magazine("Журнал: Колосок", "Животный мир", 2003);
        Book book = new Book("Книга: Война и мир", "Лев Толстой", "АСТ", 1869);
        Yearbook yearbook = new Yearbook("Ежегодник: Сад и Огород", "Садоводство", "Ранок", 2005);

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите год выпуска литературы: ");
        int foundYearOfPublic = scan.nextInt();
        System.out.println("Введите название литературы (Журнал, Книга или Ежегодник)");
        String foundLiterature = scan.next();

        if (foundYearOfPublic == magazine.getYearOfPullMonthAndYear() || foundLiterature.equalsIgnoreCase(magazine.getName())) {
            System.out.println(magazine.getName() + " | " + " Тема: " + magazine.getTopic() + " | " + magazine.getYearOfPullMonthAndYear());
        } if (foundYearOfPublic == book.getYearOfPubl() || foundLiterature.equalsIgnoreCase(book.getName())) {
            System.out.println(book.getName() + " | " + book.getAuthor() + " Издательство: " + book.getPublish() + " | " + book.getYearOfPubl());
        } if (foundYearOfPublic == yearbook.getYearOfPubl() || foundLiterature.equalsIgnoreCase(yearbook.getName())) {
            System.out.println(yearbook.getName() + " |" + " Тематика: " + yearbook.getTopic() + " | " + " Издательство: " + yearbook.getPublish() + " | " + yearbook.getYearOfPubl());
        }
    if (foundYearOfPublic != magazine.getYearOfPullMonthAndYear() && foundYearOfPublic != book.getYearOfPubl() && foundYearOfPublic != yearbook.getYearOfPubl()) {
        System.err.println("Литература выбранного года не доступна");
    }
    }

}


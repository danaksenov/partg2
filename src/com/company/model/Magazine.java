package com.company.model;

public class Magazine {
    private String name;
    private String topic;
    private int yearOfPullMonthAndYear;

    public String getName() {
        return name;
    }

    public String getTopic() {
        return topic;
    }

    public int getYearOfPullMonthAndYear() {
        return yearOfPullMonthAndYear;
    }

    public Magazine(String name, String topic, int yearOfPullMonthAndYear) {
        this.name = name;
        this.topic = topic;
        this.yearOfPullMonthAndYear = yearOfPullMonthAndYear;
    }
}

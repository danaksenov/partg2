package com.company.model;

public class Book {
    private String name;
    private String author;
    private String publish;
    private int yearOfPubl;

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublish() {
        return publish;
    }

    public int getYearOfPubl() {
        return yearOfPubl;
    }

    public Book(String name, String author, String publish, int yearOfPubl) {
        this.name = name;
        this.author = author;
        this.publish = publish;
        this.yearOfPubl = yearOfPubl;
    }
}

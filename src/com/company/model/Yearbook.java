package com.company.model;

public class Yearbook {
    private String name;
    private String topic;
    private String publish;
    private int yearOfPubl;

    public String getName() {
        return name;
    }

    public String getTopic() {
        return topic;
    }

    public String getPublish() {
        return publish;
    }

    public int getYearOfPubl() {
        return yearOfPubl;
    }

    public Yearbook(String name, String topic, String publish, int yearOfPubl) {
        this.name = name;
        this.topic = topic;
        this.publish = publish;
        this.yearOfPubl = yearOfPubl;
    }
}
